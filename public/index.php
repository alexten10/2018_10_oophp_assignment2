<?php

session_start();

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);


require __DIR__ . '/../vendor/autoload.php';

use App\Lib\Input;

$input = new Input();

// set cookie values
setcookie('assignment2', 'If you see <strong>this</strong> you are almost done!');
setcookie('test', 'BWAHAHAHAHAHAHAHA');

// Set post values
$_POST['first'] = '<strong>Bill</strong>';
$_POST['last'] = '<strong>Gates</strong>';
$_POST['hobbies'] = array('<strong>astronomy</strong>', '<strong>cycling</strong>', 
'<strong>reading</strong>');

// Set get values
$_GET['title'] = "A <em>Day</em> in the <strong>Life</strong> of Dolly Dressler";
$_GET['id'] = 12;
$_GET['cats'] = ['literature', 'sf', 'mystery'];

$_SERVER['CUSTOM_STRING'] = "You are <strong>Hacked</strong>!";

$input = new Input();


//var_dump($_POST);
//var_dump($_GET);
//var_dump($_COOKIE);
//var_dump($_SERVER);
?>

<style>

body {
	font-size: 70%
}

strong, em {
	color: #BB0000;
}

</style>


<?php

echo "<h1>Assignment 2 Output</h1>";
echo "<p>The <strong>Input</strong> class is instantiated as <strong>\$input</strong>.</p>\n\n<p>Then, the following tests are run.</p>\n\n<p>If your Input class has been created properly, your tests should generate similar output.</p>";
echo "<br /><hr /><br />";


echo "<h2>\$input->post() - raw</h2>";
echo '<pre>';
var_dump($input->post()); // raw POST
echo '</pre>';
echo "<br /><hr /><br />";


echo "<h2>\$input->post(null, true) - clean</h2>";
echo '<pre>';
var_dump($input->post(null, true)); // sanitized POST
echo "<br /><hr /><br />";


echo "<h2>\$input->post('first') - raw</h2>";
echo '<pre>';
var_dump($input->post('first')); // raw POST['first']
echo '</pre>';
echo "<br /><hr /><br />";


echo "<h2>\$input->post('first', true) - clean</h2>";
echo '<pre>';
var_dump($input->post('first', true)); // sanitized POST['first']
echo '</pre>';
echo "<br /><hr /><br />";


echo "<h2>\$input->cookie() - raw</h2>";
echo '<pre>';
var_dump($input->cookie());
echo '</pre>';
echo "<br /><hr /><br />";


echo "<h2>\$input->cookie(null, true) - clean</h2>";
echo '<pre>';
var_dump($input->cookie(null, true));
echo '</pre>';
echo "<br /><hr /><br />";


echo "<h2>\$input->cookie('assignment2') - raw</h2>";
echo '<pre>';
var_dump($input->cookie('assignment2'));
echo '</pre>';
echo "<br /><hr /><br />";


echo "<h2>\$input->cookie('assignment2', true) - clean</h2>";
echo '<pre>';
var_dump($input->cookie('assignment2', true));
echo '</pre>';
echo "<br /><hr /><br />";


echo "<h2>\$input->server() - raw</h2>";
echo '<pre>';
var_dump($input->server());
echo '</pre>';
echo "<br /><hr /><br />";


echo "<h2>\$input->server(null, true) - clean</h2>";
echo '<pre>';
var_dump($input->server(null, true));
echo '</pre>';
echo "<br /><hr /><br />";


echo "<h2>\$input->server('CUSTOM_STRING') - raw</h2>";
echo '<pre>';
var_dump($input->server('CUSTOM_STRING'));
echo '</pre>';
echo "<br /><hr /><br />";


echo "<h2>\$input->server('CUSTOM_STRING', clean) - clean</h2>";
echo '<pre>';
var_dump($input->server('CUSTOM_STRING', true));
echo '</pre>';
echo "<br /><hr /><br />";



echo "<h2>END</h2>";
