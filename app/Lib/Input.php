<?php

/*
* file Inpit.php
*created 2018-10-15
*author Alex Ten
*/

namespace App\Lib;





class Input implements IInput, IInputPost, IInputGet, IInputCookie, IInputServer
{
  
  use \App\Lib\Traits\InstanceClass;
  
  
  
  /*
  *function to get specific result from $_POST superglobal 
  *if no values are passed to this function,
  * default values will be assigned: $key = null, $clean = false
  *$key - string, which is index of $_POST  ( $_POST[$key] )
  *$clean - boolean, which is a flag to sanitize input data or return raw data
  */
  public function post($key = null, $clean = false)
  {
    //assign $_POST to an array to return results and NOT change $_POST array
    $result = Array();
    $result = $_POST;
    echo $this->sayClass();
    
    //if nothing sent to the function
    if ( ($key == null) && ($clean == false) ) {
      return $result;//returns raw result (do nothing and return)
    }//END if
    
    //if 1 parameter sent to the function is not null
    if ( ($key !== null) && ($clean == false) ) {
      return $result[$key];//returns raw value of $key index
    }//END if
    
    //if function gets 2 parameters (NOT default)
    if ( ($key !== null) && ($clean == true) ) {
      //remove html tags first, then replace special chars
      return htmlspecialchars(strip_tags($result[$key]));
    }//END if
    
    //if function gets only 2nd parameter == true
    if ( ($key == null) && ($clean == true) ) {
      foreach ($result as $key => $value) {
        if (is_string($value)) {
          $result[$key] = htmlspecialchars(strip_tags($value));
        }//END if
        //if any value is an array in a multidimentional array,
        //remove tags and replace special chars of values in that "subarray"
        elseif (is_array($value)) {
          foreach ($value as $index => $string) {
            $result[$key][$index] = htmlspecialchars(strip_tags($string));
          }//END foreach
        }//END if/elseif
        
      }//END foreach()
      return $result;
    }//END if
    
  }//END post()
  
  
  
  
  /*
  *function to get specific result from $_GET superglobal 
  *if no values are passed to this function,
  * default values will be assigned: $key = null, $clean = false
  *$key - string, which is index of $_GET  ( $_GET[$key] )
  *$clean - boolean, which is a flag to sanitize input data or return raw data
  */
  public function get($key = null, $clean = false)
  {
    //assign $_GET to an array to return results and NOT change $_GET array
    $result = Array();
    $result = $_GET;
    
    if ( ($key == null) && ($clean == false) ) {
      return $result;
    }//END if
    
    if ( ($key !== null) && ($clean == false) ) {
      return $result[$key];
    }//END if
    
    if ( ($key !== null) && ($clean == true) ) {
      //remove html tags first, then replace special chars
      return htmlspecialchars(strip_tags($result[$key]));
    }//END if
    
    //if function gets only 2nd parameter == true
    if ( ($key == null) && ($clean == true) ) {
      foreach ($result as $key => $value) {
        if (is_string($value)) {
          $result[$key] = htmlspecialchars(strip_tags($value));
        }//END if
        //if any value is an array in a multidimentional array,
        //remove tags and replace special chars of values in that "subarray"
        elseif (is_array($value)) {
          foreach ($value as $index => $string) {
            $result[$key][$index] = htmlspecialchars(strip_tags($string));
          }//END foreach
        }//END if/elseif
        
      }//END foreach()
      return $result;
    }//END if
    
  }//END get()
  
  
  
  
  /*
  *function to get specific result from $_COOKIE superglobal 
  *if no values are passed to this function,
  * default values will be assigned: $key = null, $clean = false
  *$key - string, which is index of $_COOKIE  ( $_COOKIE[$key] )
  *$clean - boolean, which is a flag to sanitize input data or return raw data
  */
  public function cookie($key = null, $clean = false)
  {
    //assign $_COOKIE to an array to return results and NOT change $_COOKIE array
    $result = Array();
    $result = $_COOKIE;
    
    if ( ($key == null) && ($clean == false) ) {
      return $result;//returns raw result (do nothing and return)
    }//END if
    
    if ( ($key !== null) && ($clean == false) ) {
      return $result[$key];//returns raw value of $key index
    }//END if
    
    if ( ($key !== null) && ($clean == true) ) {
      //remove html tags first, then replace special chars
      return htmlspecialchars(strip_tags($result[$key]));
    }//END if
    
    if ( ($key == null) && ($clean == true) ) {
      foreach ($result as $key => $value) {
        $result[$key] = htmlspecialchars(strip_tags($value));//every value becomes string(no int, float, etc)
      }//END foreach()
      return $result;
    }//END if
    
  }//END cookie()
  
  
  
  
  /*
  *function to get specific result from $_SERVER superglobal 
  *if no values are passed to this function,
  * default values will be assigned: $key = null, $clean = false
  *$key - string, which is index of $_SERVER  ( $_SERVER[$key] )
  *$clean - boolean, which is a flag to sanitize input data or return raw data
  */
  public function server($key = null, $clean = false)
  {
    //assign $_SERVER to an array to return results and NOT change $_SERVER array
    $result = Array();
    $result = $_SERVER;
    
    if ( ($key == null) && ($clean == false) ) {
      return $result;//returns raw result (do nothing and return)
    }//END if
    
    if ( ($key !== null) && ($clean == false) ) {
      return $result[$key];//returns raw value of $key index
    }//END if
    
    if ( ($key !== null) && ($clean == true) ) {
      //remove html tags first, then replace special chars
      return htmlspecialchars(strip_tags($result[$key]));
    }//END if
    
    if ( ($key == null) && ($clean == true) ) {
      foreach ($result as $key => $value) {
        $result[$key] = htmlspecialchars(strip_tags($value));//every value becomes string(no int, float, etc)
      }//END foreach()
      return $result;
    }//END if
    
  }//END server()
  
  
  
}//END class Input


